import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Author } from './author';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  constructor(private httpClient: HttpClient) { }

  public getAuthors(): Observable<Author[]>  {
    return this.httpClient.get<Author[]>("/api/authors");
  }

}
