
export interface Author {
    id: Number
    name: string
}